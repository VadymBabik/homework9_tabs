"use strict";

let menuList = document.querySelector(".tabs");
let textList = document.querySelectorAll(".tabs-content>li");
let menuTabs = document.querySelectorAll(".tabs-title");

menuList.onclick = function (eve) {
  menuTabs.forEach((elem) => {
    elem.classList.remove("active");
  });

  let target = eve.target;

  if (target.className === "tabs-title") {
    target.classList.add("active");

    textList.forEach((e) => {
      if (e.className === target.innerHTML) {
        e.style.display = "inline-block";
      } else {
        e.style.display = "none";
      }
    });
  }
};
